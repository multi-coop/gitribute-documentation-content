## No trap, just open source

Nothing really original here in the world of open source projects.

- **Code the technical backbone of the idea** ;
- **Start small and cheap, and increment around a roadmap** ;
- **Mutualize clients' needs** ;
- **Valorize our expertise, not the code** ;
