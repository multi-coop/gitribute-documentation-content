### Proprietary "no code" solutions

🚧  &nbsp; `Redaction in progress...`

| Solution                                             | Main features | Database | Pros | Cons |
| ---                                                  | ---           | ---      | ---  | --- |
| **[Airtable       ](airtable.com)**                     | forms, collaborative, SAS | Cloud | UX/UI | mandatory account |
| **[Kantree        ](https://kantree.io/)**              | forms, collaborative, SAS | Cloud | ... | mandatory account, UX/UI |
| **[Notion         ](notion.so)**                        | forms, collaborative, SAS | Cloud | UX/UI | mandatory account |
| **[Coda           ](https://coda.io/)**                 | forms, collaborative, SAS | Cloud | ... | mandatory account |
| **[GoogleSheet    ](https://google.com/sheets/about/)** | collaborative, dataviz, SAS | Cloud | UX/UI | mandatory account |
| **[Excel          ](https://office.live.com/)**         | desktop | Cloud | ... | ... |
| **[Open Data Soft ](https://www.opendatasoft.com/en/)** | SAS, platform | Cloud | ... | mandatory account, subscription costs |

> **Note** : Feel free to contribute to this benchmark if you think about solutions we would have missed. The little grey icon on the right side will send you directly [to the content's file on Github](https://github.com/multi-coop/gitribute-documentation-content/blob/main/texts/benchmark/benchmark-comparisons-proprietary-en.md).
