
# Tutorials

This section aims to give you some hints for using basic features of Gitribute.

Ideally we wouldn't want to write this section at all, given we are trying to design Gitribute as self-explanatory : a lot of tooltips are associated to the action buttons, so the user could directly have a glimpse of what every button does without having to jump to the docs.

But if we're honest, we're not 100% sure our UX design will ever be ever clear enough for everybody, so tutorials could be seen as complementary tools to guide new users.

In the illustration and tutorials below you will discover the different buttons usually accessible by a Gitribute user :

<div>
  <img
    alt="TUTORIAL-INTRO-ALL_ACTIONS"
    src="https://raw.githubusercontent.com/multi-coop/gitribute-documentation-content/main/images/tutorial/commented/tutorial-01.png"
    />
</div>
