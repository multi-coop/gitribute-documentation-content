### Éditer & contribuer à des données en ligne

<div style="border: thin solid lightgrey;">
  <img
    alt="TUTORIAL-EDITION-CSV"
    src="https://raw.githubusercontent.com/multi-coop/gitribute-documentation-content/main/images/tutorial/edition-edit-csv.png"
    />
</div>

<br>

La première des fonctionnalités de Gitribute est de permettre à toute personne de contribuer facilement à des données en ligne (_no code_), **sans avoir à créer de compte sur une plateforme** si la personne ne le souhaite pas.

🚧  &nbsp; `Redaction in progress...`

- **Éditer des données hébergées sur Github or Gitlab** (fichiers `csv`, `md`, ou `json`) ;
- **Permettre à des utilisateurs.rices non identifié.e.s de pousser des modifications sur une branche à part** et de créer automatiquement une "pull request" ;
