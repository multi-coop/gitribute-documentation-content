### Partager et valoriser vos jeux de données

Gitribute vous permet pour chaque fichier de mettre en place **un ou plusieurs widgets que d'autres personnes pourront copier et inclure dans leur propre site web**.

Même si le widget est copié, les données source resteront les mêmes. De cette façon **vous pourrez multiplier le nombre de contributeurs potentiels à votre jeu de données**.

🚧  &nbsp; `Redaction in progress...`

- **Créer un ensemble de composants modulaires** :
  - prévisualisation de fichiers `.csv`, `.md`, `.json` ou de ressources `mediawiki`, avec différents choix de vues (table, liste de fiches, ...) ;
  - interfaces d'édition ;
  - dataviz de fichiers `.csv` (barcharts, pies, ...) ;
  - préférences utilisateurs: langage, token utilisateur, ...
  - etc...
- **Intégration web modulaire**
