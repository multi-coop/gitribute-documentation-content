# Benchmark

🚧  &nbsp; `Translation in progress...`

While developping Gitribute we keep in mind there are other solutions on similar use cases.

**All those solutions - including Gitribute - come with pros and cons.**

Some are open source, other proprietary.

The vast majority of these solutions store your data in their "cloud". The open source solutions usually allow you to chose where to host your own backend server (and assuming its maintenance).

You need to pay to access most of these solutions, one way or another ("[when it's proprietary and if it's free you are the product](https://techhq.com/2018/04/facebook-if-something-is-free-you-are-the-product/)"), etc...

We like open source solutions, no question about it. But - open or not - you know what we say about **the "cloud" : it's just someone else's server**.

The data storage centralization does not cope with Gitribute main intentions, so we tried to find a way around.
