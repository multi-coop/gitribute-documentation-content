### Open source "no code" solutions

🚧  &nbsp; `Redaction in progress...`

| Solution                                                    | Main features | Database | Pros | Cons |
| ---                                                         | ---           | ---      | ---  | ---  |
| **[Gitribute   ](gittribute-docs.multi.coop)**              | widget, SPA | Git providers | ... | large datasets |
| **[Data Patch  ](https://gitlab.com/multi-coop/datapatch)** | collaborative, SAS | Cloud, PostGreSQL | ... | mandatory account, in eaaaarly developments |
| **[Gîs         ](https://gxis.codeursenliberte.fr/)**       | map, collaborative, SAS | Cloud | ... | mandatory account |
| **[NoCodb      ](https://www.nocodb.com/)**                 | forms, collaborative, SAS | Cloud, PostGreSQL | ... | mandatory account, UX/UI |
| **[Baserow     ](https://baserow.io/)**                     | forms, collaborative, SAS | Cloud | ... | mandatory account, UX/UI |
| **[FramaCalc   ](https://framacalc.org/abc/fr/)**           | collaborative, SAS | Cloud | ... | mandatory account |
| **[Libre Office](https://www.libreoffice.org/)**            | ... | Cloud | ... | mandatory account |
| **[Koumoul     ](https://koumoul.com/)**                    | open data platform | Cloud | ... | mandatory account |
| **[Mediawiki   ](https://www.mediawiki.org/wiki/MediaWiki)**| wiki | Cloud | ... | mandatory account, not adapted for table data |
| **[SemApps     ](https://semapps.org/)**                    | semantic web | Cloud | decentralized | mandatory account, complexity |
| **[ckan        ](https://ckan.org/)**                       | platform, DMS | Cloud | ... | mandatory account, complexity |

> **Note** : Feel free to contribute to this benchmark if you think about solutions we would have missed. The little grey icon on the right side will send you directly [to the content's file on Github](https://github.com/multi-coop/gitribute-documentation-content/blob/main/texts/benchmark/benchmark-comparisons-open-en.md).
