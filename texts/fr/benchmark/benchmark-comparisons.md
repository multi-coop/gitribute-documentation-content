## Comparisons

🚧  &nbsp; `Redaction in progress...`

The solutions we listed below are the ones we could think of, as close as possible to answer to the **sum of constraints** (all at once) of an "open data for all":

- **Free**, or at least very **cheap** ;
- Good **UX** ;
- Good **UI** ;
- Data **agnostic** ;
- **Integrable** in third parties wesites ;
- **Simple to deploy** ;
- **Simple to maintain** ;
- **Replicable** ;
- **No mandatory account** creation to edit data ;
- **Open source** (but let keep proprietary solutions in our radar)
